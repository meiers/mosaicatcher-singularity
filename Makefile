sing-build:
	sudo singularity --debug build mosaicatcher-1.0.img Singularity

dock-build:
	docker build -t mosaicatcher:1.0 .
push:
	docker tag mosaicatcher:1.0 titansmc/sascha-pipeline:1.0
	docker push titansmc/sascha-pipeline:1.0

login-embl:
	docker login git.embl.de:4567

dock-embl-build:
	docker build -t git.embl.de:4567/meiers/mosaicatcher-singularity:2.0 .
dock-embl-push:
	docker push git.embl.de:4567/meiers/mosaicatcher-singularity:2.0
sing-embl-build:
	singularity build mosaicatcher-singularity-2.0.img docker://git.embl.de:4567/meiers/mosaicatcher-singularity:2.0
